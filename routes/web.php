<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('siteHome');
Route::get('/Service', 'ServiceController@index')->name('siteService');
Route::get('/About', 'AboutController@index')->name('siteAbout');
Route::get('/Contacts', 'ContactController@index')->name('siteContacts');
Route::post('/Contacts/send', 'ContactController@send')->name('siteSend');


Auth::routes();

Route::group(['prefix' => 'AdminPanel', 'middleware' => 'auth'], function (){
    #Home
    Route::get('/', 'admin\HomeSliderController@index')->name('home');
    ###################################################################

    #Main_Slider
    Route::get('/Home_Slider', 'admin\HomeSliderController@index')->name('mainSlider');
    Route::get('/Home_Slider/Add', 'admin\HomeSliderController@add')->name('addMainSlider');
    Route::post('/Home_Slider/Save', 'admin\HomeSliderController@save')->name('saveMainSlider');
    Route::get('/Home_Slider/Edit/{id}', 'admin\HomeSliderController@edit')->name('editMainSlider');
    Route::post('/Home_Slider/Update/{id}', 'admin\HomeSliderController@update')->name('updateMainSlider');
    Route::post('/Home_Slider/Delete/{id}', 'admin\HomeSliderController@delete')->name('deleteMainSlider');
    ###################################################################

    #Clients_Say
    Route::get('/Clients_Say', 'admin\ClientsSayController@index')->name('clientsSay');
    Route::get('/Clients_Say/Add', 'admin\ClientsSayController@add')->name('addClientSay');
    Route::post('/Clients_Say/Save', 'admin\ClientsSayController@save')->name('saveClientSay');
    Route::get('/Clients_Say/Edit/{id}', 'admin\ClientsSayController@edit')->name('editClientSay');
    Route::post('/Clients_Say/Update/{id}', 'admin\ClientsSayController@update')->name('updateClientSay');
    Route::post('/Clients_Say/Delete/{id}', 'admin\ClientsSayController@delete')->name('deleteClientSay');
    ####################################################################

    #Clients
    Route::get('/Our_Clients', 'admin\OurClientsController@index')->name('Clients');
    Route::get('/Our_Clients/Add', 'admin\OurClientsController@add')->name('addClient');
    Route::post('/Our_Clients/Save', 'admin\OurClientsController@save')->name('saveClients');
    Route::post('/Our_Clients/delete/{id}', 'admin\OurClientsController@delete')->name('deleteClient');

    #Features
    Route::get('/Features', 'admin\FeaturesController@index')->name('Features');
    Route::get('/Features/Add', 'admin\FeaturesController@add')->name('addFeature');
    Route::post('/Features/Save', 'admin\FeaturesController@save')->name('saveFeature');
    Route::get('/Features/Edit/{id}', 'admin\FeaturesController@edit')->name('editFeature');
    Route::post('/Features/Update/{id}', 'admin\FeaturesController@update')->name('updateFeature');
    Route::post('/Features/Delete/{id}', 'admin\FeaturesController@delete')->name('deleteFeature');
    ######################################################################

    #Team
    Route::get('/Team', 'admin\TeamController@index')->name('Team');
    Route::get('/Team/Add', 'admin\TeamController@add')->name('addTeam');
    Route::post('/Team/Save', 'admin\TeamController@save')->name('saveTeam');
    Route::get('/Team/Edit/{id}', 'admin\TeamController@edit')->name('editTeam');
    Route::post('/Team/Update/{id}', 'admin\TeamController@update')->name('updateTeam');
    Route::post('/Team/Delete/{id}', 'admin\TeamController@delete')->name('deleteTeam');
    ########################################################################

    #Messsages
    Route::get('/Messages', 'admin\MessagesController@index')->name('Messages');
    Route::get('/Messages/Show/{id}', 'admin\MessagesController@show')->name('showMessage');
    Route::post('/Messages/Delete/{id}', 'admin\MessagesController@delete')->name('deleteMessage');
    Route::any('/Messages/Search', 'admin\MessagesController@search')->name('searchMessage');
    #########################################################################

    #Admins
        Route::get('/Admins', 'admin\AdminsController@index')->name('Admins');
        Route::post('/Admins/Save', 'admin\AdminsController@save')->name('saveAdmin');
        Route::post('/Admins/Delete/{id}', 'admin\AdminsController@delete')->name('deleteAdmin');
        Route::post('/Admins/Update/{id}', 'admin\AdminsController@update')->name('updateAdmin');
    #########################################################################

    #Setting
    Route::get('/Setting', 'admin\SettingController@index')->name('Setting');
    Route::post('/Setting/Update/{id}', 'admin\SettingController@update')->name('updateSetting');

});
