<?php

namespace App\Providers;

use App\Message;
use View;
use App\Setting;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $contacts = Setting::first();
        View::share('contacts', $contacts);

        $messages = count(Message::where('seen', 0)->get());
        View::share('messages', $messages);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
