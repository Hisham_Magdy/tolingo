<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        return view('contacts');
    }

    public function send(Request $request)
    {
        $contact = new Message();
        $contact->name = $request->name;
        $contact->email = $request->email;

        if($request->has('phone'))
        {
            $contact->phone = $request->phone;
        }
        $contact->subject = $request->subject;
        $contact->message = $request->message;
        if($request->hasFile('file'))
        {
            $file = $request->file;
            $filename = str_random(6). time() . $file->getClientOriginalName();
            $path = 'Uploads';
            $file->move($path, $filename);
            $contact->file = $path . '/' . $filename;
        }
        $contact->save();

        session(['success' => 'Your request has been sent successfully']);

        return redirect()->route('siteHome');
    }
}
