<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        $team = Team::all();
        return view('about', compact('team'));
    }
}
