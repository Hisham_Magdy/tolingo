<?php

namespace App\Http\Controllers\admin;

use App\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AdminsController extends Controller
{

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            if(Auth::user()->role != 1)
            {
                abort(404);
            }
            return $next($request);
        });
    }

    public function index()
    {
        $admins = User::all();
        return view('admin.admins', compact('admins'));
    }

    public function update(Request $request, $id)
    {
        $admin = User::find($id);
        if(!$admin)
        {
            abort(404);
        }
        $admin->password = bcrypt($request->password);
        $admin->update();

        session(['success' => 'Admin password has been modified successfully']);

        return redirect()->route('Admins');
    }

    public function delete($id)
    {
        if($admins = count(User::all()) > 1)
        {
            $admin = User::destroy($id);

            session(['success' => 'Admin has been destroyed successfully']);
            return redirect()->back();
        } else {
            session(['success' => 'Sorry, there is only one admin']);
            return redirect()->back();
        }

    }
}
