<?php

namespace App\Http\Controllers\admin;

use Validator;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function index()
    {
        $setting = Setting::first();
        return view('admin.setting', compact('setting'));
    }

    public function update(Request $request, $id)
    {
        $errors = Validator::make($request->all(), [
            'phone' => 'required',
            'email' => 'required',
            'location' => 'required',
        ]);

        if($errors->fails())
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }

        $setting = Setting::find($id);
        if(!$setting)
        {
            abort(404);
        }
        $setting->phone = $request->phone;
        $setting->email = $request->email;
        $setting->location = $request->location;
        $setting->facebook = $request->facebook;
        $setting->twitter = $request->twitter;
        $setting->linkedin = $request->linkedin;
        $setting->google = $request->google;
        $setting->send_email = $request->second_email;
        $setting->update();

        session(['success' => 'Setting has been updated successfully']);

        return redirect()->back();
    }
}
