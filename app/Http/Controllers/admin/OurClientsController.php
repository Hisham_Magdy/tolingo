<?php

namespace App\Http\Controllers\admin;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OurClientsController extends Controller
{
    public function index()
    {
        $clients = Client::all();
        return view('admin.clients.index', compact('clients'));
    }

    public function add()
    {
        return view('admin.clients.add');
    }

    public function save(Request $request)
    {
        foreach ($request->file as $image)
        {
            $client = new Client();
            $file = $image;
            $filename = str_random(6) . time() . $file->getClientOriginalName();
            $path = 'Uploads';
            $file->move($path, $filename);
            $client->image = $path . '/' . $filename;
            $client->save();
        }
    }

    public function delete($id)
    {
        $client = Client::destroy($id);

        session(['success' => 'Client has been destroyed successfully']);

        return redirect()->back();
    }
}
