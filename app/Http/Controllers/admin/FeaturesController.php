<?php

namespace App\Http\Controllers\admin;

use App\Feature;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeaturesController extends Controller
{
    public function index()
    {
        $features = Feature::all();

        return view('admin.features.index', compact('features'));
    }

    public function add()
    {
        return view('admin.features.add');
    }

    public function save(Request $request)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required',
            'feature_content' => 'required',
            'image' => 'required|image|mimes:jpg,JPG,png,PNG,jpeg,JPEG,svg,SVG',
        ]);

        if($errors->fails())
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }

        $feature = new Feature();
        $feature->title = $request->title;
        $feature->content = $request->feature_content;

        #image
        $file = $request->image;
        $filename = str_random(6) . time() . $file->getClientOriginalName();
        $path = 'Uploads';
        $file->move($path, $filename);
        $feature->image = $path . '/' . $filename;
        #######################

        $feature->save();

        session(['success' => 'Feature has been added successfully']);

        return redirect()->route('Features');
    }

    public function edit($id)
    {
        $feature = Feature::find($id);
        if(!$feature)
        {
            abort(404);
        }
        return view('admin.features.edit', compact('feature'));
    }

    public function update(Request $request, $id)
    {
        $errors = Validator::make($request->all(), [
            'title' => 'required',
            'feature_content' => 'required',
            'image' => 'image|mimes:jpg,JPG,png,PNG,jpeg,JPEG,svg,SVG',
        ]);

        if($errors->fails())
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }

        $feature = Feature::find($id);
        $feature->title = $request->title;
        $feature->content = $request->feature_content;

        #image
        if($request->hasFile('image'))
        {
            $file = $request->image;
            $filename = str_random(6) . time() . $file->getClientOriginalName();
            $path = 'Uploads';
            $file->move($path, $filename);
            $feature->image = $path . '/' . $filename;
        }
        #######################

        $feature->update();

        session(['success' => 'Feature has been modified successfully']);

        return redirect()->route('Features');
    }


    public function delete($id)
    {
//        $feature = Feature::destroy($id);

        session(['success' => 'Feature has been destroyed successfully']);

        return redirect()->route('Features');
    }
}
