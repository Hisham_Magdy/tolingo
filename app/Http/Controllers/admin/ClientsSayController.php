<?php

namespace App\Http\Controllers\admin;

use App\Client_Say;
use Illuminate\Contracts\Session\Session;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientsSayController extends Controller
{
    public function index()
    {
        $saies = Client_Say::all();
        return view('admin.clients_say.index', compact('saies'));
    }

    public function add()
    {
        return view('admin.clients_say.add');
    }

    public function save(Request $request)
    {
        $errors = Validator::make($request->all(), [
            'name' => 'required',
            'position' => 'required',
            'client_content' => 'required',
            'image' => 'image|mimes:jpg,JPG,png,PNG,jpeg,JPEG,svg,SVG',
        ]);

        if($errors->fails())
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }

        $client = new Client_Say();
        $client->name = $request->name;
        $client->position = $request->position;
        $client->content = $request->client_content;

        #image
        if($request->hasFile('image')) {
            $file = $request->image;
            $filename = str_random(6) . time() . $file->getClientOriginalName();
            $path = 'Uploads';
            $file->move($path, $filename);
            $client->image = $path . '/' . $filename;
        }
        else {
            $client->image = 'Uploads/avatar.gif';
        }
        #######################
//        dd('done');
        $client->save();

        session(['success' => 'Content has been added successfully']);
        return redirect()->route('clientsSay');
    }

    public function edit($id)
    {
        $say = Client_Say::find($id);
        if(!$say)
        {
            abort(404);
        }
        return view('admin.clients_say.edit', compact('say'));
    }

    public function update(Request $request, $id)
    {
        $errors = Validator::make($request->all(), [
            'name' => 'required',
            'position' => 'required',
            'client_content' => 'required',
            'image' => 'image|mimes:jpg,JPG,png,PNG,jpeg,JPEG,svg,SVG',
        ]);

        if($errors->fails())
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }

        $client = Client_Say::find($id);
        if(!$client)
        {
            abort(404);
        }
        $client->name = $request->name;
        $client->position = $request->position;
        $client->content = $request->client_content;

        #image
        if($request->hasFile('image'))
        {
            $file = $request->image;
            $filename = str_random(6) . time() . $file->getClientOriginalName();
            $path = 'Uploads';
            $file->move($path, $filename);
            $client->image = $path . '/' . $filename;
        }
        #######################
//        dd('done');
        $client->update();

        session(['success' => 'Content has been modified successfully']);
        return redirect()->route('clientsSay');
    }

    public function delete($id)
    {
        $client = Client_Say::destroy($id);

        session(['success' => 'Content has been destroyed successfully']);
        return redirect()->route('clientsSay');
    }


}
