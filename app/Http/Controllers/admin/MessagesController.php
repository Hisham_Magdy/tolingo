<?php

namespace App\Http\Controllers\admin;

use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessagesController extends Controller
{
    public function index()
    {
        $allMessages = count(Message::all());
        $messages = Message::orderBy('seen')->paginate(30);
        return view('admin.messages.index', compact('messages', 'allMessages'));
    }

    public function show($id)
    {
        $message = Message::find($id);
        if(!$message)
        {
            abort(404);
        }
        $message->seen = 1;
        $message->update();

        return view('admin.messages.show', compact('message'));
    }

    public function delete($id)
    {
        $message = Message::destroy($id);

        session(['success' => 'Message has been destroyed successfully']);
        return redirect()->route('Messages');
    }

    public function search()
    {
        $messages = Message::where('name', 'LIKE', '%' . $_GET['query'] . '%')->orWhere('subject', 'LIKE', '%' . $_GET['query'] . '%')->orWhere('message', 'LIKE', '%' . $_GET['query'] . '%')->orderBy('seen')->get();

        return view('admin.messages.index', compact('messages'));
    }
}
