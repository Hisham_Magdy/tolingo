<?php

namespace App\Http\Controllers\admin;

use Validator;
use App\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    public function index()
    {
        $team = Team::all();
        return view('admin.team.index', compact('team'));
    }

    public function add()
    {
        return view('admin.team.add');
    }

    public function save(Request $request)
    {
        $errors = Validator::make($request->all(), [
            'name' => 'required',
            'position' => 'required',
            'team_content' => 'required',
        ]);

        if($errors->fails())
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }

        $team = new Team();
        $team->name = $request->name;
        $team->position = $request->position;
        $team->content = $request->team_content;
        $team->save();

        session(['success' => 'Member has been added successfully']);

        return redirect()->route('Team');
    }

    public function edit($id)
    {
        $team = Team::find($id);
        if(!$team)
        {
            abort(404);
        }
        return view('admin.team.edit', compact('team'));
    }

    public function update(Request $request, $id)
    {
        $errors = Validator::make($request->all(), [
            'name' => 'required',
            'position' => 'required',
            'team_content' => 'required',
        ]);

        if($errors->fails())
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }

        $team = Team::find($id);
        if(!$team)
        {
            abort(404);
        }
        $team->name = $request->name;
        $team->position = $request->position;
        $team->content = $request->team_content;
        $team->update();

        session(['success' => 'Member has been modified successfully']);

        return redirect()->route('Team');
    }

    public function delete($id)
    {
//        $team = Team::destroy($id);

        session(['success' => 'Member has been destroyed successfully']);
        return redirect()->route('Team');
    }

}
