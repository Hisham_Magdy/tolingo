<?php

namespace App\Http\Controllers\admin;

use App\Main_Slider;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeSliderController extends Controller
{
    public function index()
    {
        $sliders = Main_Slider::all();
        return view('admin.main_slider.index', compact('sliders'));
    }

    public function add()
    {
        return view('admin.main_slider.add');
    }

    public function save(Request $request)
    {
        $errors = Validator::make($request->all(),[
            'title' => 'required',
            'image' => 'required|image|mimes:jpg,JPG,png,PNG,jpeg,JPEG,svg,SVG',
        ]);
        if($errors->fails())
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }
        $slider = new Main_Slider();
        $slider->content = $request->title;
        $file = $request->image;
        $filename = str_random(6) . time() . $file->getClientOriginalName();
        $path = 'Uploads';
        $file->move($path, $filename);
        $slider->image = $path . '/' . $filename;
        $slider->save();

        session(['success' => 'Slider has been add successfully']);

        return redirect()->route('mainSlider');
    }

    public function edit($id)
    {
        $slider = Main_Slider::find($id);
        if(!$slider)
        {
            abort(404);
        }
        return view('admin.main_slider.edit', compact('slider'));
    }

    public function update(Request $request, $id)
    {
        $errors = Validator::make($request->all(),[
            'title' => 'required',
            'image' => 'image|mimes:jpg,JPG,png,PNG,jpeg,JPEG,svg,SVG',
        ]);
        if($errors->fails())
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }
        $slider = Main_Slider::find($id);
        $slider->content = $request->title;
        if($request->has('image'))
        {
            $file = $request->image;
            $filename = str_random(6) . time() . $file->getClientOriginalName();
            $path = 'Uploads';
            $file->move($path, $filename);
            $slider->image = $path . '/' . $filename;
        }
        $slider->update();

        session(['success' => 'Slider has been modified successfully']);

        return redirect()->route('mainSlider');
    }

    public function delete($id)
    {
        $slider = Main_Slider::destroy($id);

        session(['success' => 'Slider has been deleted successfully']);
        return redirect()->back();
    }
}
