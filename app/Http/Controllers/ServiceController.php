<?php

namespace App\Http\Controllers;

use App\Feature;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function index()
    {
        $allFeaturs = Feature::all();
        $features = array_chunk(json_decode($allFeaturs), ceil(count($allFeaturs) / 2));

//        $features = array_chunk($allfeatures, ceil(count($allfeatures) / 2));
//        return $features;
        return view('service', compact('features'));
    }
}
