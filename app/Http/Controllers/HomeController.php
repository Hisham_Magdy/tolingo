<?php

namespace App\Http\Controllers;

use App\Client;
use App\Client_Say;
use App\Main_Slider;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $mainSliders = Main_Slider::all();
        $clientSaies = Client_Say::all();
        $clients = Client::all();

        return view('index', compact('mainSliders', 'clientSaies', 'clients'));
    }
}
