$(document).ready(function () {
    
    var up = $('.fa-chevron-up'),
        sideSocials = $('.side-socials'),
        mainPage = $('html, body');
    
    //fixed navbar
    $(window).scroll(function () {
        if ($(this).scrollTop() >=50){
            $('.navbar').addClass('navbar-fixed-top');
            $('.navbar').css('box-shadow', '1px 1px 10px 2px gray');
        } else {
            $('.navbar').removeClass('navbar-fixed-top');
            $('.navbar').css('box-shadow', '0px 0px 0px 0px transparent');
        }
    });
    
    //tooltip
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });
    
    //side-socials & up fading
    $(window).scroll(function () {
        if ($(this).scrollTop() >= 100){
            sideSocials.fadeIn();
        } else {
            sideSocials.fadeOut();
        } if ($(this).scrollTop() >= 800){
            up.fadeIn(500);
        } else {
            up.fadeOut(500);
        }
    });
    
    //testimonials Slider
    $('.carousel').carousel();
    
    //animation wow liberary
    new WOW().init();
    
    //owl carousel (clients logo)
    $('.owl-carousel').owlCarousel({
        autoplay:true,
        loop:true,
        dots: false,
        nav: true,
        margin: 20,
        responsive:{
            0:{items:1},
            600:{items:3},
            1000:{items:5}
            },
        center:true,
        slideBy:2,
        autoplayTimeout:5000,
      });
    
    //up
    up.click(function () {
        mainPage.animate({
            scrollTop: 0
        },500);
    });
    
    //down
    $('.down').click(function () {
        mainPage.animate({
            scrollTop: $('.we-offer').offset().top
        });
    });
   
});

