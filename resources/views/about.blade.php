@extends('layouts.master')

@section('title', 'About')

@section('content')

    <!--============ Start identity ============-->
    <section class="about-identity">
        <div class="container-fluid"><!--Start Container-->
            <h2>About Us</h2>
        </div><!--End Container-->
    </section>
    <!--============ End identity ============-->

    <!--============ Start words-about ============-->
    <section class="words-about">
        <div class="container"><!--Start Container-->
            <h2 class="wow slideInLeft" data-duration="1s"><span>A few</span> words about us</h2>
            <p class="wow slideInRight" data-duration="1s">
                TOLINGO Company was founded on —and continues to operate on— the core values of integrity, respect, and discipline. Our individual employees each embody these values in their tireless commitment to serving our clients. Our corporate culture embodies them as well: we foster an atmosphere of dedicated, intensely focused camaraderie in which each individual works as part of a tight team, yet freely helps others. Even our corporate finances reflect our ideals.
                We are fully committed to providing the highest quality translations to our clients and superior “service beyond words.”
                Our goal is to provide a great customer experience and make it easy to work with us. We help our clients in areas beyond translation, by providing complementary services.
            </p>

            <div class="row"><!--Start row-->
                <div class="col-md-6 col-xs-12">
                    <h2 class="h3 wow fadeInUp" data-duration="1s"><span>Our</span> Mission</h2>
                    <p class="wow fadeInUp" data-duration="2s">
                        Enable our clients reach their global markets by providing them with high-quality localization and translation services and fast turnaround. We maximize high quality and efficiency by employing native talent with advanced language, technical, and project management skills, and by using innovative software and communication technologies often unique to our company.
                    </p>
                </div>

                <div class="col-md-6 col-xs-12">
                    <h2 class="h3 wow fadeInUp" data-duration="1s"><span>Quality</span> Assurance</h2>
                    <p class="wow fadeInUp" data-duration="2s">
                        What sets TOLINGO Multilingual apart from the competition is our strict quality focus that combines multiple quality metrics into a single Quality Management System. TOLINGO QMS measures all quality assurance processes, storing the results for future reference and traceability enabling TOLINGO to monitor and use statistical data for feedback and quality control.
                    </p>
                </div>
            </div><!--End row-->

        </div><!--End Container-->
    </section>
    <!--============ End words-about ============-->

    <!--============ Start our-team ============-->
    <section class="our-team">
        <div class="overlay"><!--Start overlay-->
            <div class="container"><!--Start Container-->
                <div class="row"><!--Start row-->

                    <h3 class="text-center"><span>Meet</span> our team</h3>

                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        @php($i = 0)
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            @foreach($team as $item)
                                <li data-target="#carousel-example-generic" data-slide-to="{{$i}}" class="{{$i == 0 ? 'active' : ' '}}"></li>
                                @php($i++)
                            @endforeach
                        </ol>
                        @php($i = 0)
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">

                            @foreach($team as $value)
                                <div class="item {{$i == 0 ? 'active' : ' '}}">
                                    <div class="item-info">
                                        <p>
                                            {{$value->content}}
                                        </p>
                                        <div class="item-name">
                                            <h4>{{$value->name}}</h4>
                                            <span>{{$value->position}}</span>
                                        </div>
                                    </div>
                                </div>
                                @php($i++)
                            @endforeach
                        </div>
                    </div>

                </div><!--End row-->
            </div><!--End Container-->
        </div><!--End overlay-->
    </section>
    <!--============ End our-team ============-->

    <!--============ Start join ============-->
    <section class="join">
        <div class="container"><!--Start Container-->
            <div class="join-group wow fadeInUp" data-duration="2s">
                <div class="front-join">
                    <h3><span>Join</span> our Team</h3>
                    <p>
                        TOLINGO specializes in providing translation and localization solutions to a wide variety of industries. We are staffed and equipped with experts who can convert your products from and into all commercial languages.
                        TOLINGO places the utmost level of detail and attention on service, quality and innovation. Our primary goal is to make you and your international end-users successful! We support over 50 languages and dialects.
                        Our community comprises of more than 20 enthusiastic, friendly full-time Linguists and over 250 translators.
                    </p>
                </div>
                <div class="behind-join">
                    <div class="overlay">
                        <p>
                            If you believe in yourself that you have required qualifications and experiences to join our team, whether for freelance or a full-time job, please send us your qualifications and experiences as well as a copy of your CV and copy of your best previously accomplished translations to the following <span>email: {{$contacts->send_email}}</span>
                        </p>
                    </div>
                </div>
            </div>
        </div><!--End Container-->
    </section>
    <!--============ End join ============-->

@endsection