<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.header')
</head>
<body class="about">
<!--============ Start nav ============-->

@include('includes.nav')

<!--============ End nav ============-->

<!--============ Start Content ============-->
@yield('content')
<!--============ End Content ============-->


<!--============ Start footer ============-->
<footer>
    <p class="text-center">All Copy Rights {{date('Y')}} &copy; Reserved to Tolingo Cpmpany, Designed and Developed by <span><b>Sindbad</b></span></p>
</footer>
<!--============ End footer ============-->

<!--============ Start up ============-->
<i class="fa fa-chevron-up fa-fw text-center"></i>
<!--============ End up ============-->

<!--============ Start price-form ============-->
<section class="price-form hidden-xs">

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">
        <!--                <img src="img/Quote.png" alt="Quote">-->
        Quote
    </button>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="container"><!--Start Container-->
                    <div class="row"><!--Start row-->
                        @include('includes.quote')
                    </div><!--End row-->
                </div><!--End Container-->
            </div>
        </div>
    </div>
</section>
<!--============ End price-form ============-->

<!--============ Strat loading ============-->
<div id="pre-loader">
    <div class="sk-cube-grid">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
</div>
<!--============ End loading ============-->

<!--============ jQuery liberary ============-->

<!--============ bootstrap JS ============-->
@include('includes.scripts')
</body>
</html>