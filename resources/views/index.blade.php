@extends('layouts.master')

@section('title', 'Home')

@section('content')

    <section class="home-slider" dir="ltr">

        <div class="demo-2" dir="ltr">

            <!-- Codrops top bar -->

            <div id="slider" class="sl-slider-wrapper" dir="ltr">

                <div class="sl-slider">

                    @foreach($mainSliders as $mainSlider)
                        <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                            <div class="sl-slide-inner">
                                <div class="bg-img bg-img-1" style="background-image: url({{asset($mainSlider->image)}}); background-size: cover"></div>
                                <h2 class="shadow-head-slide"></h2>
                                <blockquote><p>{{$mainSlider->content}}</p></blockquote>
                            </div>
                        </div>
                    @endforeach

                </div><!-- /sl-slider -->

                <nav id="nav-dots" class="nav-dots">
                    @php($i = 1)
                    @foreach($mainSliders as $mainSlider)
                        <span class="{{$i == 1 ? 'nav-dot-current' : ' '}}"></span>
                        @php($i++)
                    @endforeach
                </nav>

            </div><!-- /slider-wrapper -->

        </div>

        <!--
                    <div class="down">
                        <i class="fa fa-chevron-down fa-fw"></i>
                    </div>
        -->
    </section>
    <!--============ End home-slider ============-->

    <!--============ Start we-offer ============-->
    <section class="we-offer">
        <h2 class="text-center h1"><span>We</span> offer you</h2>
        <div class="container"><!--Start Container-->
            <div class="row"><!--Start row-->

                <div class="col-md-4 col-xs-12">

                    <div class="offer media wow slideInLeft" data-wow-duration="1s">
                        <div class="media-left">
                            <img class="media-object" src="img//we-offer-img/response.png" alt="response" width="64" height="64">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Immediate response</h4>
                            <p>
                                We are very responsive during the day.  You can reach us 24 hours per day, 07 days per week.
                            </p>
                        </div>
                    </div>

                    <div class="offer media wow slideInLeft" data-wow-duration="1s">
                        <div class="media-left">
                            <img class="media-object" src="img/we-offer-img/speed.png" alt="speed" width="64" height="64">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Maximum speed whenever necessary</h4>
                            <p>
                                We usually accept long projects, yet we can handle the very urgent files.
                            </p>
                        </div>
                    </div>

                    <div class="offer media wow slideInLeft" data-wow-duration="1s">
                        <div class="media-left">
                            <img class="media-object" src="img/we-offer-img/quality.png" alt="quality" width="64" height="64">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Service and quality adapted to your needs</h4>
                            <p>
                                Our translators' target is what the customer needs and wants, that is why we keep a guideline booklet for every single customer.
                            </p>
                        </div>
                    </div>

                </div>

                <div class="col-md-4 col-xs-12">

                    <div class="offer media wow fadeInUpBig" data-wow-duration="1s">
                        <div class="media-left">
                            <img class="media-object" src="img/we-offer-img/on-time.png" alt="on-time" width="64" height="64">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Punctual delivery</h4>
                            <p>
                                We assure you that we firmly stick to deadlines and sometimes we can deliver even before them.
                            </p>
                        </div>
                    </div>

                    <div class="offer media wow fadeInUpBig" data-wow-duration="1s">
                        <div class="media-left">
                            <img class="media-object" src="img/we-offer-img/branches.png" alt="branches">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Our place</h4>
                            <p>
                                You will find us beside you in anywhere you live.
                            </p>
                        </div>
                    </div>

                    <div class="offer media wow fadeInUpBig" data-wow-duration="1s">
                        <div class="media-left">
                            <img class="media-object" src="img/we-offer-img/native.png" alt="native" width="64" height="64">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Translations carried out by native speakers</h4>
                            <p>
                                Most of our translators are native speaker, so our translations normally sound coherent and correct.
                            </p>
                        </div>
                    </div>

                </div>

                <div class="col-md-4 col-xs-12">

                    <div class="offer media wow slideInRight" data-wow-duration="1s">
                        <div class="media-left">
                            <img class="media-object" src="img/we-offer-img/experience.png" alt="experience" width="64" height="64">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Experience in languages and business</h4>
                            <p>
                                Our vast experience in translation along with business makes it easy for you to do it.
                            </p>
                        </div>
                    </div>

                    <div class="offer media wow slideInRight" data-wow-duration="1s">
                        <div class="media-left">
                            <img class="media-object" src="img/we-offer-img/price.png" alt="Competitive prices" width="64" height="64">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Competitive prices</h4>
                            <p>
                                we can do all your works at affordable price with taking good care of the translation quality.
                            </p>
                        </div>
                    </div>

                    <div class="offer media wow slideInRight" data-wow-duration="1s">
                        <div class="media-left">
                            <img class="media-object" src="img/we-offer-img/partner.png" alt="international partner" width="64" height="64">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading" style="font-size: 15px;letter-spacing: 0">A trustworthy and professional international partner</h4>
                            <p>
                                Not only can we offer quality written translation and proofreading, but also we can provide you with well-trained interpreters, who would deliver your messages.
                            </p>
                        </div>
                    </div>

                </div>

            </div><!--End row-->
        </div><!--End Container-->
    </section>
    <!--============ End we-offer ============-->

    <!--============ Start testi ============-->
    <section class="testi">
        <div class="overlay"><!--Start overlay-->
            <div class="container"><!--Start Container-->
                <h3 class="text-center"><span>Our</span> clients say</h3>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        @php($i = 0)
                        @foreach($clientSaies as $clientSay)
                            <li data-target="#carousel-example-generic" data-slide-to="{{$i}}" class="{{$i == 0 ? 'active' : ''}}"></li>
                            @php($i++)
                        @endforeach
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        @php($i = 0)
                        @foreach($clientSaies as $clientSay)
                            <div class="item {{$i == 0 ? 'active' : '' }}">
                                <div class="item-info">
                                    <p>
                                        {{$clientSay->content}}
                                    </p>
                                    <div class="item-name">
                                        <img src="{{asset($clientSay->image)}}" alt="{{$clientSay->image}}">
                                        <h4>{{$clientSay->name}}</h4>
                                        <span>{{$clientSay->position}} </span>
                                    </div>
                                </div>
                            </div>
                            @php($i++)
                        @endforeach
                    </div>

                </div>

            </div><!--End Container-->
        </div><!--End overlay-->
    </section>
    <!--============ End testi ============-->

    <!--============ Start our-clients ============-->
    <section class="our-clients">
        <div class="container"><!--Start Container-->

            <div class="popular-product">
                <h2 class="text-center"><span>Our</span> Clients</h2>
                <div class="owl-carousel owl-theme">

                    @foreach($clients as $client)
                        <div class="items-1">
                            <img src="{{asset($client->image)}}" alt="{{asset($client->image)}}">
                        </div>
                    @endforeach

                </div>
            </div>

        </div><!--End Container-->
    </section>
    <!--============ End our-clients ============-->

    <!--============ Start latest-work ============-->
    <section class="latest-work">
        <div class="overlay">
            <div class="container"><!--Start Contaier-->
                <h2 class="text-center"><span>Latest</span> work</h2>
                <div class="row"><!--Start row-->

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="project wow fadeInUpBig" data-wow-duration="1s">
                            <img  class="img-responsive" src="img/latest-img-1.jpg" alt="latest-1">
                            <div class="pro-info">
                                <h4>POL</h4>
                                <p>
                                    A 35000-word English-Arabic project of a multinational company's policy regarding guidelines, instructions, and regulations of the Human Resources department.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="project wow fadeInDownBig" data-wow-duration="1s">
                            <img class="img-responsive" src="img/latest-img-2.jpg" alt="latest-2">
                            <div class="pro-info">
                                <h4>START</h4>
                                <p>
                                    A 42000-word Arabic-English documentary project for an UAE company. Our translation helped the company express its vision and mission to a business committee and thus win a prize.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="project wow fadeInUpBig" data-wow-duration="1s">
                            <img class="img-responsive" src="img/latest-img-3.jpg" alt="latest-3">
                            <div class="pro-info">
                                <h4>Tender Spain</h4>
                                <p>
                                    Spanish-English and Spanish-Arabic project of 15 tenders of several services between Spain and Egypt.
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="project wow fadeInDownBig" data-wow-duration="1s">
                            <img class="img-responsive" src="img/latest-img-4.jpg" alt="latest-4">
                            <div class="pro-info">
                                <h4>Univ Flyer</h4>
                                <p>
                                    a flyer translated into 20 languages for a restaurant in Qatar
                                </p>
                            </div>
                        </div>
                    </div>

                </div><!--End row-->
            </div><!--End Contaier-->
        </div>
    </section>
    <!--============ End latest-work ============-->
@endsection