@extends('layouts.master')

@section('title', 'Service')

@section('content')

    <!--============ Start identity ============-->
    <section class="contact-identity">
        <div class="container-fluid"><!--Start Container-->
            <h2>Service</h2>
        </div><!--End Container-->
    </section>
    <!--============ End identity ============-->

    <!--============ Start languages ============-->
    <section class="languages">
        <div class="container"><!--Start Container-->
            <div class="row"><!--Start row-->

                <div class="col-md-6 col-xs-12">
                    <div class="multi-language wow fadeInLeftBig" data-duration="1s">
                        <h2><span>Languages</span> we support
                            <hr>
                        </h2>
                        <p>
                            Besides English as a second language, Dynamic Language Services provides instruction in a variety of languages including..
                        </p>
                        <h4><span>Asian</span> Languages</h4>
                        <ul class="list-unstyled list-inline">
                            <li>Japanese</li>
                            <li>Mandarin</li>
                            <li>Thai</li>
                            <li>Korean</li>
                            <li>Vietnamese</li>
                            <li>Urdu</li>
                            <li>Malayalam</li>
                            <li>Malay</li>
                        </ul>
                        <h4><span>Germanic</span> Languages</h4>
                        <ul class="list-unstyled list-inline">
                            <li>German</li>
                            <li>Swedish</li>
                        </ul>
                        <h4><span>Near</span> Eastern Languages</h4>
                        <ul class="list-unstyled list-inline">
                            <li>Arabic</li>
                            <li>Hebrew</li>
                            <li>Turkish</li>
                            <li>Farsi</li>
                            <li>Azerbaijani</li>
                        </ul>
                        <h4><span>Romance</span> Languages</h4>
                        <ul class="list-unstyled list-inline">
                            <li>French</li>
                            <li>Spanish</li>
                            <li>Italian</li>
                            <li>Portuguese</li>
                        </ul>
                        <h4><span>Slavic</span> Languages</h4>
                        <ul class="list-unstyled list-inline">
                            <li>Russian</li>
                            <li>Czech</li>
                            <li>Polish</li>
                            <li>Ukranian</li>
                            <li>Azerbaijani</li>
                        </ul>
                        <h4><span>Cl</span>assical</h4>
                        <ul class="list-unstyled list-inline">
                            <li>Latin</li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-6 col-xs-12">
                    <div class="tree-language wow fadeInUpBig" data-duration="1s">
                        <img class="img-responsive" src="{{asset('img/service/language-tree.png')}}" alt="language-tree" width="374" height="416">
                    </div>
                </div>

            </div><!--End row-->
        </div><!--End Container-->
    </section>
    <!--============ End languages ============-->

    <!--============ Start our-service ============-->
    <section class="service">
        <div class="overlay"><!--Start overlay-->
            <div class="container"><!--Start Container-->
                <h2><span>Our</span> Services
                    <hr>
                </h2>
                <div class="row"><!--Start row-->

                    <div class="col-sm-6 col-xs-12">
                        <div class="single-service wow fadeInLeftBig" data-duration="1s"><!--Start single-service-->
                            <h3><span>Translation</span> Services</h3>
                            <p>
                                TOLINGO is a leading provider of both digital and document translations, dedicated to providing you with the highest standard language solutions at a competitive price.
                                <br>
                                Whether it’s for a large multinational client or an SMB, we offer only the most professional translation services and use only the most qualified industry translators.
                                <br>
                                If you need legal document translation or a technical document translation, TOLINGO is regularly entrusted by clients to provide high quality translations. We take pride in our customer retention rate of 98%, delivering top quality service and a great experience every time.
                            </p>
                        </div><!--End single-service-->
                    </div>

                    <div class="col-sm-6 col-xs-12">
                        <div class="single-service wow fadeInRightBig" data-duration="1s"><!--Start single-service-->
                            <h3><span>Proofreading</span> Services</h3>
                            <p>
                                TOLINGO offers comprehensive translation proofreading services, in order to ensure that the translation is consistent with client glossaries or specialist usage. As with any proofreading services, the translation will also be screened for grammatical or spelling errors by a second mother-tongue translator.
                                <br>
                                Our translation proofreading service offers peace of mind that your website, publication or marketing material is linguistically flawless. The proofreader will ensure it adheres to the stylistic and tonal requirements for your project, whilst keeping the target audience in mind. This second professional opinion and cultural consultation is then returned to the original translator, who gives the document final approval.
                            </p>
                        </div><!--End single-service-->
                    </div>

                </div><!--End row-->
            </div><!--End Container-->
        </div><!--End overlay-->
    </section>
    <!--============ End our-service ============-->

    <!--============ Start features ============-->
    <section class="features">
        <div class="overlay">
            <div class="container"><!--Start Container-->

                <h2><span>our</span> Features
                    <hr>
                </h2>
                <div class="row"><!--Start row-->

                    @foreach($features as $feature)
                        <div class="col-md-6 col-xs-12">

                            @foreach($feature as $item)
                                <div class="group-feats wow flipInX" data-duration="1s">
                                    <div class="front-feat">
                                        <img src="{{asset($item->image)}}" alt="{{$item->title}}" width="256" height="265">
                                        <h5 class="h4 text-center">{{$item->title}}</h5>
                                    </div>
                                    <div class="behind-feat">
                                        <p>
                                            {{$item->content}}
                                        </p>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    @endforeach


                </div><!--End row-->

            </div><!--End Container-->
        </div>
    </section>
    <!--============ End features ============-->

@endsection