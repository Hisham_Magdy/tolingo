<script src="{{asset('js/bootstrap.js')}}"></script>
<!--============ Wow JS ============-->
<script src="{{asset('js/wow.min.js')}}"></script>
<!-- Owl Carousel Slider -->
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<!--========== Slider home JS ==========-->
<script type="text/javascript" src="{{asset('js/modernizr.custom.79639.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.ba-cond.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.slitslider.js')}}"></script>
<script type="text/javascript">
    $(function() {

        var Page = (function() {

            var $nav = $( '#nav-dots > span' ),
                slitslider = $( '#slider' ).slitslider( {
                    onBeforeChange : function( slide, pos ) {

                        $nav.removeClass( 'nav-dot-current' );
                        $nav.eq( pos ).addClass( 'nav-dot-current' );

                    }
                } ),

                init = function() {

                    initEvents();

                },
                initEvents = function() {

                    $nav.each( function( i ) {

                        $( this ).on( 'click', function( event ) {

                            var $dot = $( this );

                            if( !slitslider.isActive() ) {

                                $nav.removeClass( 'nav-dot-current' );
                                $dot.addClass( 'nav-dot-current' );

                            }

                            slitslider.jump( i + 1 );
                            return false;

                        } );

                    } );

                };

            return { init : init };

        })();

        Page.init({
            interval: 5000
        });
        /**
         * Notes:
         *
         * example how to add items:
         */

        /*

         var $items  = $('<div class="sl-slide sl-slide-color-2" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1"><div class="sl-slide-inner bg-1"><div class="sl-deco" data-icon="t"></div><h2>some text</h2><blockquote><p>bla bla</p><cite>Margi Clarke</cite></blockquote></div></div>');

         // call the plugin's add method
         ss.add($items);

         */

        //message box

    });

</script>
<!--============ custom JS ============-->
<script src="{{asset('js/main-custom.js')}}"></script>
<!-- Toastr -->
<script src="{{asset('admin/js/plugins/toastr/toastr.min.js')}}"></script>

@if(session('success'))
    <script>
        $(document).ready(function() {
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('{{session('success')}}');

            }, 1300);

        });
    </script>
    {{Session::forget('success')}}
@endif

@yield('scripts')