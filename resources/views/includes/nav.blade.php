<div class="top-connection">
    <div class="container-fluid"><!--Start Container-->

    <ul class="phone list-unstyled list-inline navbar-left">
        @php
            $phones = explode('-', $contacts->phone);
            $emails = explode('-', $contacts->email);
        @endphp

        <li>
            <i class="fa fa-envelope fa-fw"></i>
            {{$emails[0]}}
        </li>
        <li>
            <i class="fa fa-phone fa-fw"></i>
            {{$phones[0]}}
        </li>
    </ul>

    <ul class="social-items list-unstyled list-inline navbar-right">
        <li>
            @if($contacts->facebook)
                <a target="_blank" href="{{$contacts->facebook}}">
                    <i class="fa fa-facebook fa-fw" data-toggle="tooltip" data-placement="bottom" title="facebook"></i>
                </a>
            @endif
            @if($contacts->twitter)
                <a target="_blank" href="{{$contacts->twitter}}">
                    <i class="fa fa-twitter fa-fw" data-toggle="tooltip" data-placement="bottom" title="Twitter"></i>
                </a>
            @endif
            @if($contacts->linkedin)
                <a target="_blank" href="{{$contacts->linkedin}}">
                    <i class="fa fa-linkedin fa-fw" data-toggle="tooltip" data-placement="bottom" title="linkedin"></i>
                </a>
            @endif
            @if($contacts->google)
                <a target="_blank" href="{{$contacts->google}}">
                    <i class="fa fa-google-plus fa-fw" data-toggle="tooltip" data-placement="bottom" title="Google plus"></i>
                </a>
            @endif
        </li>
    </ul>

</div><!--End Container-->
</div>


<!--============ End nav ============-->

<!--============ Start side-socials ============-->
@include('includes.socialSide')
<!--============ End side-socials ============-->

<!--============ Start nav ============-->
<nav class="navbar navbar-default">
    <div class="container-fluid"><!--Start Container-->
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{route('siteHome')}}">
                <img src="{{asset('img/logo.png')}}" alt="logo">
            </a>
        </div>
        @php
            $route = Route::current();
        @endphp
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="a-parent" href="{{route('siteHome')}}">
                        Home
                        <div class="a-child  {{$route->getName() === 'siteHome' ? 'active-a' : ' '}}"></div>
                    </a>
                </li>
                <li>
                    <a class="a-parent" href="{{route('siteService')}}">
                        Service
                        <div class="a-child {{$route->getName() === 'siteService' ? 'active-a' : ' '}}"></div>
                    </a>
                </li>
                <li>
                    <a class="a-parent" href="{{route('siteAbout')}}">About Us
                        <div class="a-child {{$route->getName() === 'siteAbout' ? 'active-a' : ' '}}"></div>
                    </a>
                </li>
                <li>
                    <a class="a-parent" href="{{route('siteContacts')}}">Contact Us
                        <div class="a-child {{$route->getName() === 'siteContacts' ? 'active-a' : ' '}}"></div>
                    </a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!--End Container-->
</nav>
<!--============ End nav ============-->

