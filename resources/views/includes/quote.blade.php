<form action="{{route('siteSend')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <input type="text" name="name" placeholder="Your name">
        </div>

        <div class="input-group">
            <input type="email" name="email" placeholder="Email">
            <input type="hidden" name="subject" value="Quote Request">
        </div>

        <div class="input-group">
            <input type="tel" name="phone" placeholder="Telephone number">
        </div>
    </div>

    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <textarea name="message" placeholder="Message"></textarea>
        </div>
    </div>

    <div class="col-md-6 col-xs-12">
        <div class="input-group">
            <input type="file" name="file">
        </div>
    </div>
    <input type="submit" value="send">
</form>