<div class="side-socials">
    <ul class="list-unstyled">
        @if($contacts->facebook)
            <li>
                <a target="_blank" href="{{$contacts->facebook}}">
                    <i class="fa fa-facebook fa-fw"></i>
                </a>
            </li>
        @endif
        @if($contacts->twitter)
            <li>
                <a target="_blank" href="{{$contacts->twitter}}">
                    <i class="fa fa-twitter fa-fw"></i>
                </a>
            </li>
        @endif
        @if($contacts->linkedin)
            <li>
                <a target="_blank" href="https://www.linkedin.com/in/tolingo-for-translation-services-673a1a94/">
                    <i class="fa fa-linkedin fa-fw"></i>
                </a>
            </li>
        @endif
        @if($contacts->google)
            <li>
                <a target="_blank" href="{{$contacts->google}}">
                    <i class="fa fa-google-plus fa-fw"></i>
                </a>
            </li>
        @endif
    </ul>
</div>