<meta charset="utf-8">
<meta name="description" content="Tolingo is a translation company, wich supports all languages weather English, Russian, German, Turkish ...">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Tolingo | @yield('title')</title>
<link rel="icon" href="{{asset('img/fave-icon.png')}}" type="image/png">
<!--============== Raleway Google Fonts ==============-->
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<!--============== Animate CSS ==============-->
<link rel="stylesheet" href="{{asset('css/animate.css')}}">
<!--============== fontAweasome CSS ==============-->
<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
<!-- Owl carousel style sheet -->
<link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
<!-- Owl carousel themes default -->
<link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
<!--================ CSS for slider ================-->
<link rel="stylesheet" href="{{asset('css/demo.css')}}" />
<link rel="stylesheet" href="{{asset('css/style-slider.css')}}" />
<link rel="stylesheet" href="{{asset('css/custom.css')}}" />
<!--============== bootstrap CSS ==============-->
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
<!--============== myStyle CSS ==============-->
<link rel="stylesheet" href="{{asset('css/main-style.css')}}">
<noscript>
    <link rel="stylesheet" href="{{asset('css/styleNoJS.css')}}" />
</noscript>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script>
    $(document).ready(function () {

    });
    window.addEventListener('load',function () {
        var pa = document.getElementById('pre-loader');
        document.body.removeChild(pa);
    });
</script>

<!-- Toastr style -->
{!! Html::style('admin/css/plugins/toastr/toastr.min.css') !!}