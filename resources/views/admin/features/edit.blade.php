@extends('admin.layout.master')

@section('title', 'Add New Main Slider')

@section('styles')
    {!! Html::style('admin/css/plugins/cropper/cropper.min.css') !!}
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Features:</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('Features')}}">Features</a>
                </li>
                <li class="active">
                    <strong>Edit: {{$feature->title}}</strong>
                </li>
            </ol>
        </div>
        {{--<div class="col-sm-8">--}}
        {{--<div class="title-action">--}}
        {{--<a href="" class="btn btn-primary">This is action area</a>--}}
        {{--</div>--}}
        {{--</div>--}}
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInRight">
                {{--<div class="">--}}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Edit: <strong>{{$feature->title}}</strong></h5>

                            </div>
                            <div class="ibox-content">
                                <form method="post" class="form-horizontal" action="{{route('updateFeature', $feature->id)}}" enctype="multipart/form-data">
                                    {{csrf_field()}}

                                    <div class="form-group {{$errors->has('title') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('title')? ' animated shake' : ''}}">Title:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="title" value="{{$feature->title}}">
                                            @if($errors->has('title'))<span style="color:red;">{{$errors->first('title')}}</span>@endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('feature_content') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('feature_content')? ' animated shake' : ''}}">Content:</label>
                                        <div class="col-sm-10">
                                            <textarea type="text" class="form-control" name="feature_content" value="{{old('feature_content')}}">{{$feature->content}}</textarea>
                                            @if($errors->has('feature_content'))<span style="color:red;">{{$errors->first('feature_content')}}</span>@endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('image') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('image')? ' animated shake' : ''}}">Image:</label>
                                        <div class="col-sm-10">
                                            <input type="file" name="image">
                                            @if($errors->has('image'))<span style="color:red;">{{$errors->first('image')}}</span>@endif
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <a href="{{route('Features')}}" class="btn btn-white">Cancel</a>
                                            <button class="btn btn-primary submit" type="submit">Save changes</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection

