@extends('admin.layout.master')

{{--@section('styles')--}}
    {{--{!! Html::style('admin/css/plugins/cropper/cropper.min.css') !!}--}}
{{--@endsection--}}

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Home Sliders:</h2>
            <ol class="breadcrumb">
                {{--<li>--}}
                    {{--<a href="index.html">Main Slider</a>--}}
                {{--</li>--}}
                {{--<li class="active">--}}
                    {{--<strong>Main Slider</strong>--}}
                {{--</li>--}}
            </ol>
        </div>
        <div class="col-sm-8">
            <div class="title-action">
                <a href="{{route('addMainSlider')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInRight">
                {{--<div class="">--}}
                <div class="row">
                        <div class="col-lg-12">

                            <div class="ibox float-e-margins">
                                @foreach($sliders as $slider)
                                <div class="col-md-6">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>{{$slider->content}}</h5>
                                            <div class="ibox-tools">
                                                <a href="{{route('editMainSlider', $slider->id)}}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a data-toggle="modal" data-target="#myModal{{$slider->id}}">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <figure>
                                                <img src="{{asset($slider->image)}}" style="width: 100%; height: 500px">
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                                {{--Modal start--}}
                                    <div class="modal inmodal in" id="myModal{{$slider->id}}" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;"><div class="modal-backdrop  in"></div>
                                        <div class="modal-dialog">
                                            <div class="modal-content animated flipInY">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                    <h4 class="modal-title" style="color: red">Delete!</h4>
                                                    {{--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>--}}
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure you want to delete this content ?!</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <form action="{{route('deleteMainSlider', $slider->id)}}" method="post">
                                                        {{csrf_field()}}
                                                        <a class="btn btn-white" data-dismiss="modal">No</a>
                                                        <button type="submit" class="btn btn-primary">Yes</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {{--Modal end--}}
                                @endforeach
                            </div>

                        </div>

                </div>


                </div>
            {{--</div>--}}
        </div>
    </div>
@endsection

