@extends('admin.layout.master')

@section('title', 'Add New Main Slider')

@section('styles')
    {!! Html::style('admin/css/plugins/cropper/cropper.min.css') !!}
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Setting</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Home</a>
                </li>
                <li class="active">
                    <strong>Setiing</strong>
                </li>
            </ol>
        </div>
        {{--<div class="col-sm-8">--}}
        {{--<div class="title-action">--}}
        {{--<a href="" class="btn btn-primary">This is action area</a>--}}
        {{--</div>--}}
        {{--</div>--}}
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInRight">
                {{--<div class="">--}}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Add New <strong>Team Member</strong></h5>

                            </div>
                            <div class="ibox-content">
                                <form method="post" class="form-horizontal" action="{{route('updateSetting', $setting->id)}}" enctype="multipart/form-data">
                                    {{csrf_field()}}

                                    <div class="form-group {{$errors->has('phone') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('phone')? ' animated shake' : ''}}">Phone:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="phone" value="{{$setting->phone}}">
                                            @if($errors->has('phone'))<span style="color:red;">{{$errors->first('phone')}}</span>@endif
                                        </div>
                                    </div>


                                    <div class="form-group {{$errors->has('email') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('email')? ' animated shake' : ''}}">Email:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="email" value="{{$setting->email}}">
                                            @if($errors->has('email'))<span style="color:red;">{{$errors->first('email')}}</span>@endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('location') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('location')? ' animated shake' : ''}}">Location Link:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="location" value="{{$setting->location}}">
                                            @if($errors->has('location'))<span style="color:red;">{{$errors->first('location')}}</span>@endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('facebook') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('facebook')? ' animated shake' : ''}}">Facebook Link:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="facebook" value="{{$setting->facebook}}" placeholder="ex: http://example.com/index~">
                                            @if($errors->has('facebook'))<span style="color:red;">{{$errors->first('facebook')}}</span>@endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('twitter') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('twitter')? ' animated shake' : ''}}">Twitter Link:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="twitter" value="{{$setting->twitter}}" placeholder="ex: http://example.com/index~">
                                            @if($errors->has('twitter'))<span style="color:red;">{{$errors->first('twitter')}}</span>@endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('linkedin') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('linkedin')? ' animated shake' : ''}}">Linkedin Link:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="linkedin" value="{{$setting->linkedin}}" placeholder="ex: http://example.com/index~">
                                            @if($errors->has('linkedin'))<span style="color:red;">{{$errors->first('linkedin')}}</span>@endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('google') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('linkedin')? ' animated shake' : ''}}">Google+ Link:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="google" value="{{$setting->google}}" placeholder="ex: http://example.com/index~">
                                            @if($errors->has('google'))<span style="color:red;">{{$errors->first('google')}}</span>@endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('second_email') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('second_email')? ' animated shake' : ''}}">Which email to be requests sent?!:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="second_email" value="{{$setting->send_email}}" placeholder="ex: email@email.com">
                                            @if($errors->has('second_email'))<span style="color:red;">{{$errors->first('second_email')}}</span>@endif
                                        </div>
                                    </div>


                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <a href="{{route('home')}}" class="btn btn-white">Cancel</a>
                                            <button class="btn btn-primary submit" type="submit">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection

