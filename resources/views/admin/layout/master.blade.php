<!DOCTYPE html>
<html>

<head>
    @include('admin.includes.header')
</head>

<body>
<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        @include('admin.includes.navebar')
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    {{--<form role="search" class="navbar-form-custom" method="post" action="search_results.html">--}}
                        {{--<div class="form-group">--}}
                            {{--<input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">--}}
                        {{--</div>--}}
                    {{--</form>--}}
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                    <form method="post" action="{{route('logout')}}">
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-outline btn-default btn-lg" style="border: 0">
                            <i class="fa fa-sign-out"></i> Log out
                        </button>
                    </form>
                    </li>
                </ul>
            </nav>
        </div>
       {{--Start Page Content--}}
        @yield('content')
       {{--End Page Content--}}
        <div class="footer">
            <div>
                Copyright <strong>Sindbad</strong> Company &copy; {{date('Y')}}
            </div>
        </div>

    </div>
</div>

{{--Scripts--}}
@include('admin.includes.scripts')
{{--Scripts--}}
</body>
</html>
