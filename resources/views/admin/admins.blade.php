@extends('admin.layout.master')

@section('styles')
{!! Html::style('admin/css/plugins/switchery/switchery.css') !!}
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Team:</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Home</a>
                </li>
                <li class="active">
                    <strong>Admins</strong>
                </li>
            </ol>
        </div>
        <div class="col-sm-8">
            <div class="title-action">
                <a id="addNew" class="btn btn-primary" data-toggle="modal" data-target="#addModal"><i class="fa fa-plus"></i> Add New</a>
                {{--Modal start--}}
                    <div class="modal inmodal in" id="addModal" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;"><div class="modal-backdrop  in"></div>
                        <div class="modal-dialog">
                            <div class="modal-content animated flipInY">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                    {{--<h4 class="modal-title">Add New Admin:</h4>--}}
                                    <strong class="font-bold">Add New Admin:</strong>
                                </div>
                                <form method="post" class="form-horizontal" action="{{route('register')}}" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        {{csrf_field()}}
                                        <div class="form-group {{$errors->has('name') ? ' has-error': ' '}}">
                                            <label class="col-sm-2 control-label {{$errors->has('name')? ' animated shake' : ''}}">Name:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                                @if($errors->has('name'))<span style="color:red;">{{$errors->first('name')}}</span>@endif
                                            </div>
                                        </div>

                                        <div class="form-group {{$errors->has('email') ? ' has-error': ' '}}">
                                            <label class="col-sm-2 control-label {{$errors->has('email')? ' animated shake' : ''}}">Email:</label>
                                            <div class="col-sm-10">
                                                <input type="email" class="form-control" name="email" value="{{old('email')}}">
                                                @if($errors->has('email'))<span style="color:red;">{{$errors->first('email')}}</span>@endif
                                            </div>
                                        </div>

                                        {{--<div class="form-group {{$errors->has('role') ? ' has-error': ' '}}">--}}
                                            {{--<label class="col-sm-2 control-label {{$errors->has('email')? ' animated shake' : ''}}">Email:</label>--}}
                                            {{--<div class="col-sm-10">--}}
                                                {{--<input type="checkbox" class="js-switch" checked />--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        <div class="form-group {{$errors->has('password') ? ' has-error': ' '}}">
                                            <label class="col-sm-2 control-label {{$errors->has('password')? ' animated shake' : ''}}">Password:</label>
                                            <div class="col-sm-10">
                                                <div class="input-group">
                                                    <input id="addPassword" type="text" class="form-control" name="password">
                                                    <span class="input-group-btn">
                                                        <button id="generate" type="button" class="btn btn-primary">Generate!</button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>
                                            {{--<div class="form-group">--}}
                                                {{--<div class="col-sm-4 col-sm-offset-2">--}}
                                                    {{--<a href="{{route('clientsSay')}}" class="btn btn-white">Cancel</a>--}}
                                                    {{--<button class="btn btn-primary submit" type="submit">Save changes</button>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}

                                    </div>

                                    <div class="modal-footer">
                                            <a class="btn btn-danger" data-dismiss="modal">Close</a>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                {{--Modal end--}}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInRight">
                {{--<div class="">--}}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Role</th>
                                    <th>email</th>
                                    <th>Joined since</th>
                                    <th>Edit/Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($admins as $admin)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$admin->name}}</td>
                                        <td>{{$admin->role > 0 ? 'Super Admin' : 'Admin'}}</td>
                                        <td>{{$admin->email}}</td>
                                        <td>{{$admin->created_at->diffForHumans()}}</td>
                                        <td>
                                            <button class="btn btn-outline btn-primary dim" data-toggle="modal" data-target="#editModal{{$admin->id}}"><i class="fa fa-edit"></i></button>
                                            <button class="btn btn-outline btn-danger dim" type="button" data-toggle="modal" data-target="#deleteModal{{$admin->id}}"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    @php($i++)

                                    {{--Delete Modal start--}}
                                    <div class="modal inmodal in" id="deleteModal{{$admin->id}}" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;"><div class="modal-backdrop  in"></div>
                                        <div class="modal-dialog">
                                            <div class="modal-content animated flipInY">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                    <h4 class="modal-title" style="color: red">Delete!</h4>
                                                    {{--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>--}}
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure you want to delete this content ?!</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <form action="{{route('deleteAdmin', $admin->id)}}" method="post">
                                                        {{csrf_field()}}
                                                        <a class="btn btn-white" data-dismiss="modal">No</a>
                                                        <button type="submit" class="btn btn-primary">Yes</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--Modal end--}}

                                    {{--Update Modal start--}}
                                    <div class="modal inmodal in" id="editModal{{$admin->id}}" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;"><div class="modal-backdrop  in"></div>
                                        <div class="modal-dialog">
                                            <div class="modal-content animated flipInY">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                                    {{--<h4 class="modal-title" style="color: red">Delete!</h4>--}}
                                                    <strong class="font-bold">Change Password:</strong>
                                                </div>
                                                <form action="{{route('updateAdmin', $admin->id)}}" method="post">
                                                    {{csrf_field()}}
                                                    <div class="modal-body">
                                                        <div class="form-group {{$errors->has('password') ? ' has-error': ' '}}">
                                                            <label class="col-sm-2 control-label {{$errors->has('password')? ' animated shake' : ''}}">Password:</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <input id="updatePassword" type="text" class="form-control" name="password">
                                                                    <span class="input-group-btn">
                                                            <button id="updategenerate" type="button" class="btn btn-primary">Generate!</button>
                                                        </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a class="btn btn-danger" data-dismiss="modal">Close</a>
                                                        <button type="submit" class="btn btn-primary">Update</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    {{--Modal end--}}
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>


            </div>
            {{--</div>--}}
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Custom and plugin javascript -->
    <script src="{{asset('admin/js/inspinia.js')}}"></script>
    <script src="{{asset('admin/js/plugins/pace/pace.min.js')}}"></script>
    <!-- Switchery -->
    <script src="{{asset('admin/js/plugins/switchery/switchery.js')}}"></script>

    <script>
        $(document).ready( function () {
            $('#generate').click( function () {
                var x = Math.random().toString(36).substring(7) + Math.random().toString(36).substring(7);
                $('#addPassword').val(x);
            });

            $('#updategenerate').click( function () {
                var x = Math.random().toString(36).substring(7) + Math.random().toString(36).substring(7);
                $('#updatePassword').val(x);
            });
            @if(count($errors) > 0)
                $('#addNew').click();
            @endif
        });
    </script>


@endsection
