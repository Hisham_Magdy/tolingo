
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="icon" href="{{asset('img/fave-icon.png')}}" type="image/png">

<title>Tolingo Dashboaed | @yield('title')</title>

{!! Html::style('admin/css/bootstrap.min.css') !!}
{!! Html::style('admin/font-awesome/css/font-awesome.css') !!}

<!-- Toastr style -->
{!! Html::style('admin/css/plugins/toastr/toastr.min.css') !!}

<!-- Gritter -->
{!! Html::style('admin/js/plugins/gritter/jquery.gritter.css') !!}

{!! Html::style('admin/css/animate.css') !!}
{!! Html::style('admin/css/style.css') !!}

<style>
    textarea{
        height: 150px!important;
        width: 100%!important;
    }
</style>
@yield('styles')