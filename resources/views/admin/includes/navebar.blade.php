<div class="sidebar-collapse">
    <ul class="nav" id="side-menu">
        <li class="nav-header">
            <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="{{asset('img/fave-icon.png')}}" />
                             </span>
                <a href="{{route('siteHome')}}">
                    <span class="clear">
                     <span class="block m-t-xs">
                            <strong class="font-bold">{{Auth::user()->name}}</strong>
                     </span>
                    </span>
                </a>
            </div>
            <div class="logo-element">
                <a href="{{route('siteHome')}}">
                    <img alt="image" class="img-circle" src="{{asset('img/fave-icon.png')}}" />
                </a>
            </div>
        </li>
        @php
            $route = Route::current();
        @endphp
        <li class="{{$route->getName() == 'mainSlider' || $route->getName() == 'home' ? 'active' : ' '}}">
            <a href="{{route('mainSlider')}}"><i class="fa fa-photo"></i> <span class="nav-label">Home Sliders</span></a>
        </li>

        <li class="{{$route->getName() == 'clientsSay' || $route->getName() == 'Clients' ? 'active' : ' '}}">
            <a><i class="fa fa-users"></i> <span class="nav-label">Clients</span> <span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li class="{{$route->getName() == 'clientsSay' ? 'active' : ' '}}"><a href="{{route('clientsSay')}}">Clients Say</a></li>
                <li class="{{$route->getName() == 'Clients' ? 'active' : ' '}}"><a href="{{route('Clients')}}">Our Clients</a></li>
            </ul>
        </li>

        <li class="{{$route->getName() == 'Features' ? 'active' : ' '}}">
            <a href="{{route('Features')}}"><i class="fa fa-list-alt"></i> <span class="nav-label">Features</span></a>
        </li>

        <li class="{{$route->getName() == 'Team' ? 'active' : ' '}}">
            <a href="{{route('Team')}}"><i class="fa fa-users"></i> <span class="nav-label">Team</span></a>
        </li>

        <li class="{{$route->getName() == 'Messages' ? 'active' : ' '}}">
            <a href="{{route('Messages')}}"><i class="fa fa-envelope-o"></i>
                <span class="nav-label">Messages</span>
                {{--@if($messages > '0')--}}
                    <span class="label label-info pull-right">{{$messages}}</span>
                {{--@endif--}}
            </a>
        </li>
        {{--{{var_dump(\Illuminate\Support\Facades\Auth::user()->role)}}--}}
        @if(Auth::user()->role > 0)
            <li class="{{$route->getName() == 'Admins' ? 'active' : ' '}}">
                <a href="{{route('Admins')}}"><i class="fa fa-user-secret"></i> <span class="nav-label">Admins</span></a>
            </li>
        @endif

        <li class="{{$route->getName() == 'Setting' ? 'active' : ' '}}">
            <a href="{{route('Setting')}}"><i class="fa fa-gear"></i> <span class="nav-label">Setting</span></a>
        </li>
    </ul>

</div>