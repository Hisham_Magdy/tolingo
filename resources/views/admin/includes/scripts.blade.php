<!-- Mainly scripts -->
{!! Html::script('admin/js/jquery-2.1.1.js') !!}
{!! Html::script('admin/js/bootstrap.min.js') !!}
{!! Html::script('admin/js/plugins/metisMenu/jquery.metisMenu.js') !!}
{!! Html::script('admin/js/plugins/slimscroll/jquery.slimscroll.min.js') !!}

<!-- Flot -->
{!! Html::script('admin/js/plugins/flot/jquery.flot.js') !!}
{!! Html::script('admin/js/plugins/flot/jquery.flot.tooltip.min.js') !!}
{!! Html::script('admin/js/plugins/flot/jquery.flot.spline.js') !!}
{!! Html::script('admin/js/plugins/flot/jquery.flot.resize.js') !!}
{!! Html::script('admin/js/plugins/flot/jquery.flot.pie.js') !!}

<!-- Peity -->
{!! Html::script('admin/js/plugins/peity/jquery.peity.min.js') !!}
{!! Html::script('admin/js/demo/peity-demo.js') !!}

<!-- Custom and plugin javascript -->
<script src="{{asset('admin/js/inspinia.js')}}"></script>
<script src="{{asset('admin/js/plugins/pace/pace.min.js')}}"></script>
<script src="{{asset('admin/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- jQuery UI -->
<script src="{{asset('admin/js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

<!-- GITTER -->
<script src="{{asset('admin/js/plugins/gritter/jquery.gritter.min.js')}}"></script>

<!-- Sparkline -->
<script src="{{asset('admin/js/plugins/sparkline/jquery.sparkline.min.js')}}"></script>

<!-- Sparkline demo data  -->
<script src="{{asset('admin/js/demo/sparkline-demo.js')}}"></script>

<!-- ChartJS-->
<script src="{{asset('admin/js/plugins/chartJs/Chart.min.js')}}"></script>

<!-- Toastr -->
<script src="{{asset('admin/js/plugins/toastr/toastr.min.js')}}"></script>

@if(session('success'))
    <script>
        $(document).ready(function() {
            setTimeout(function() {
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('{{session('success')}}');

            }, 1300);

        });
    </script>
   {{Session::forget('success')}}
@endif

@yield('scripts')