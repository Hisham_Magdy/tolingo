@extends('admin.layout.master')

@section('title', 'Add New Main Slider')

@section('styles')
    <link href="{{asset('admin/css/plugins/blueimp/css/blueimp-gallery.min.css')}}" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Our Client Say</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('Clients')}}">Clients</a>
                </li>
                <li class="active">
                    <strong>Add new</strong>
                </li>
            </ol>
        </div>
        <div class="col-sm-8">
        <div class="title-action">
        <a href="{{route('addClient')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add New </a>
        </div>
        </div>
    </div>
    <div class="row">

        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInUp">
                <ul class="notes">
                    @foreach($clients as $client)
                        <li>
                            <div  style="background-color: white; box-shadow: 0px">
                                <img width="175" height="175" src="{{asset($client->image)}}">
                                <a data-toggle="modal" data-target="#myModal{{$client->id}}"><i class="fa fa-trash-o fa-2x" style="color: red"></i></a>
                            </div>
                        </li>

                        {{--Modal start--}}
                        <div class="modal inmodal in" id="myModal{{$client->id}}" tabindex="-1" role="dialog" aria-hidden="false" style="display: none;"><div class="modal-backdrop  in"></div>
                            <div class="modal-dialog">
                                <div class="modal-content animated flipInY">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" style="color: red">Delete!</h4>
                                        {{--<small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>--}}
                                    </div>
                                    <div class="modal-body">
                                        <p>Are you sure you want to delete this content ?!</p>
                                    </div>
                                    <div class="modal-footer">
                                        <form action="{{route('deleteClient', $client->id)}}" method="post">
                                            {{csrf_field()}}
                                            <a class="btn btn-white" data-dismiss="modal">No</a>
                                            <button type="submit" class="btn btn-primary">Yes</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--Modal end--}}
                    @endforeach

                </ul>
            </div>
        </div>


        {{--<div class="col-lg-12">--}}
            {{--<div class="wrapper wrapper-content animated fadeInRight">--}}
                {{--<div class="">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-lg-12">--}}
                        {{--<div class="ibox float-e-margins">--}}

                            {{--<div class="ibox-content">--}}

                                {{--<h2>Our Clients Gallery</h2>--}}

                                {{--<div class="lightBoxGallery">--}}
                                    {{--@foreach($clients as $client)--}}
                                        {{--<a href="{{asset($client->image)}}" title="Image from Unsplash" data-gallery=""><img width="200" height="200" src="{{asset($client->image)}}"></a>--}}
                                {{--@endforeach--}}

                                {{--<!-- The Gallery as lightbox dialog, should be a child element of the document body -->--}}
                                    {{--<div id="blueimp-gallery" class="blueimp-gallery">--}}
                                        {{--<div class="slides"></div>--}}
                                        {{--<h3 class="title"></h3>--}}
                                        {{--<a class="prev">‹</a>--}}
                                        {{--<a class="next">›</a>--}}
                                        {{--<a class="close">×</a>--}}
                                        {{--<a class="play-pause"></a>--}}
                                        {{--<ol class="indicator"></ol>--}}
                                    {{--</div>--}}

                                {{--</div>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}


                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
@endsection

@section('scripts')
    <!-- Custom and plugin javascript -->
    <script src="{{asset('admin/js/inspinia.js')}}"></script>
    <script src="{{asset('admin/js/plugins/pace/pace.min.js')}}"></script>

    <!-- blueimp gallery -->
    <script src="{{asset('admin/js/plugins/blueimp/jquery.blueimp-gallery.min.js')}}"></script>



@endsection

