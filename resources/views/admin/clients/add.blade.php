@extends('admin.layout.master')

@section('title', 'Add New Main Slider')

@section('styles')
    <link href="{{asset('admin/css/plugins/dropzone/basic.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/plugins/dropzone/dropzone.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Our Client Say</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('mainSlider')}}">Main Slider</a>
                </li>
                <li class="active">
                    <strong>Add new</strong>
                </li>
            </ol>
        </div>
        {{--<div class="col-sm-8">--}}
        {{--<div class="title-action">--}}
        {{--<a href="" class="btn btn-primary">This is action area</a>--}}
        {{--</div>--}}
        {{--</div>--}}
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInRight">
                {{--<div class="">--}}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Add New <strong>Slider</strong></h5>

                            </div>
                            <div class="ibox-content">
                                <form id="my-awesome-dropzone" class="dropzone" action="{{route('saveClients')}}" method="post">
                                    {{csrf_field()}}
                                    <div class="dropzone-previews"></div>
                                    <button type="submit" class="btn btn-primary pull-right">Upload!</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Custom and plugin javascript -->
    <script src="{{asset('admin/js/inspinia.js')}}"></script>
    <script src="{{asset('admin/js/plugins/pace/pace.min.js')}}"></script>

    <!-- DROPZONE -->
    <script src="{{asset('admin/js/plugins/dropzone/dropzone.js')}}"></script>


    <script>
        $(document).ready(function(){

            Dropzone.options.myAwesomeDropzone = {

                autoProcessQueue: false,
                uploadMultiple: true,
                parallelUploads: 100,
                maxFiles: 100,

                // Dropzone settings
                init: function() {
                    var myDropzone = this;

                    this.element.querySelector("button[type=submit]").addEventListener("click", function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        myDropzone.processQueue();
                    });
                    this.on("sendingmultiple", function() {
                    });
                    this.on("successmultiple", function(files, response) {
                    });
                    this.on("errormultiple", function(files, response) {
                    });
                }

            }

        });
    </script>

@endsection

