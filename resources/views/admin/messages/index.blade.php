@extends('admin.layout.master')

@section('styles')
{!! Html::style('admin/css/plugins/cropper/cropper.min.css') !!}
{!! Html::style('admin/css/style.css') !!}
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Team:</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Home</a>
                </li>
                <li class="active">
                    <strong>Messages</strong>
                </li>
            </ol>
        </div>
        {{--<div class="col-sm-8">--}}
            {{--<div class="title-action">--}}
                {{--<a href="{{route('addTeam')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInRight">
                {{--<div class="">--}}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="mail-box-header">

                            <form method="get" action="{{route('searchMessage')}}" class="pull-right mail-search">
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" name="query" placeholder="Search email">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary">
                                            Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <h2>
                                @if($_GET && $_GET['query'])
                                    Search result: "{{$_GET['query']}}"
                                @else
                                    inbox ({{$allMessages}})
                                @endif
                            </h2>
                            <div class="mail-tools tooltip-demo m-t-md">
                                <div class="btn-group pull-right">
                                    @if(!$_GET)
                                        {{$messages->links()}}
                                    @endif
                                </div>
                                @php
                                    $route = Route::current();
                                @endphp
                                <a href="{{route($route->getName())}}" class="btn btn-white btn-sm"><i class="fa fa-refresh"></i> Refresh</a>
                            </div>
                        </div>
                        <div class="mail-box">

                            <table class="table table-hover table-mail">
                                <tbody>
                                @foreach($messages as $message)
                                    <tr class="{{$message->seen > 0 ? 'read' : 'unread' }}">
                                        <td class="check-mail">
                                            {{--<input type="checkbox" class="i-checks">--}}
                                        </td>
                                        <td class="mail-ontact"><a href="{{route('showMessage', $message->id)}}">{{$message->name}}</a></td>
                                        <td class="mail-subject"><a href="{{route('showMessage', $message->id)}}">{{$message->subject}}</a></td>
                                        <td class="">@if($message->file)<i class="fa fa-paperclip"></i>@endif</td>
                                        <td class="text-right mail-date">{{$message->created_at->diffForHumans()}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>


            </div>
            {{--</div>--}}
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Custom and plugin javascript -->
    <script src="{{asset('admin/js/inspinia.js')}}"></script>
    <script src="{{asset('admin/js/plugins/pace/pace.min.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('admin/js/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
@endsection
