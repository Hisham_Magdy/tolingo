@extends('admin.layout.master')

@section('styles')
    {!! Html::style('admin/css/plugins/cropper/cropper.min.css') !!}
    {!! Html::style('admin/css/style.css') !!}
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Messages:</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Home</a>
                </li>
                <li>
                    <a href="{{route('Messages')}}">Messages</a>
                </li>
                <li class="active">
                    <strong>Show Message</strong>
                </li>
            </ol>
        </div>
        {{--<div class="col-sm-8">--}}
        {{--<div class="title-action">--}}
        {{--<a href="{{route('addTeam')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a>--}}
        {{--</div>--}}
        {{--</div>--}}
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInRight">
                {{--<div class="">--}}
                <div class="row">
                    <div class="col-lg-12">

                        {{--<div class="col-lg-9 animated fadeInRight">--}}
                            <div class="mail-box-header">
                                <div class="pull-right tooltip-demo">
                                    <form action="{{route('deleteMessage', $message->id)}}" method="post">
                                        {{csrf_field()}}
                                        <button type="submit" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Move to trash"><i class="fa fa-trash-o"></i> Remove </button>
                                    </form>
                                </div>
                                <h2>
                                    Message From: "<strong>{{$message->name}}</strong>"
                                </h2>
                                <div class="mail-tools tooltip-demo m-t-md">


                                    <h3>
                                        <span class="font-noraml">Subject: </span>{{$message->subject}}.
                                    </h3>
                                    <h5>
                                        <span class="pull-right font-noraml">{{$message->created_at->toDayDateTimeString()}}</span>
                                        <span class="font-noraml">From: {{$message->email}}</span> <br><br>
                                        <span class="font-noraml">Phone: {{$message->phone ? $message->phone : '-'}}</span>
                                    </h5>
                                </div>
                            </div>
                            <div class="mail-box">


                                <div class="mail-body">
                                    <p>
                                        {{$message->message}}
                                    </p>
                                </div>
                                @if($message->file)
                                    <div class="mail-attachment">
                                        <p>
                                            <span><i class="fa fa-paperclip"></i> There is 1 attachment </span>
                                        </p>

                                        <div class="attachment">
                                            <div class="file-box">
                                                <div class="file">
                                                    <a href="{{asset($message->file)}}">
                                                        <span class="corner"></span>

                                                        <div class="icon">
                                                            <i class="fa fa-file"></i>
                                                        </div>
                                                        <div class="file-name">
                                                            @php
                                                            $filename = explode('/', $message->file);
                                                            echo $filename[1];
                                                            @endphp
                                                            <br>
                                                            <small>Added: Jan 11, 2014</small>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                @endif
                                <div class="clearfix"></div>


                            </div>
                        {{--</div>--}}

                    </div>
                </div>
            </div>
            {{--</div>--}}
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Custom and plugin javascript -->
    <script src="{{asset('admin/js/inspinia.js')}}"></script>
    <script src="{{asset('admin/js/plugins/pace/pace.min.js')}}"></script>

    <!-- iCheck -->
    <script src="{{asset('admin/js/plugins/iCheck/icheck.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
@endsection
