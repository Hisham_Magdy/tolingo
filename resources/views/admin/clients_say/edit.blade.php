@extends('admin.layout.master')

@section('title', 'Add New Main Slider')

@section('styles')
    {!! Html::style('admin/css/plugins/cropper/cropper.min.css') !!}
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>Our Client Say</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('clientsSay')}}">Clients Say</a>
                </li>
                <li class="active">
                    Edit: <strong>{{$say->name}}</strong>
                </li>
            </ol>
        </div>
        {{--<div class="col-sm-8">--}}
        {{--<div class="title-action">--}}
        {{--<a href="" class="btn btn-primary">This is action area</a>--}}
        {{--</div>--}}
        {{--</div>--}}
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInRight">
                {{--<div class="">--}}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Edit: <strong>{{$say->name}}</strong></h5>

                            </div>
                            <div class="ibox-content">
                                <form method="post" class="form-horizontal" action="{{route('saveClientSay')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="form-group {{$errors->has('name') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('name')? ' animated shake' : ''}}">Name:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="name" value="{{$say->name}}">
                                            @if($errors->has('name'))<span style="color:red;">{{$errors->first('name')}}</span>@endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('position') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('position')? ' animated shake' : ''}}">Position:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="position" value="{{$say->position}}">
                                            @if($errors->has('position'))<span style="color:red;">{{$errors->first('position')}}</span>@endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('client_content') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('client_content')? ' animated shake' : ''}}">What Client Said?:</label>
                                        <div class="col-sm-10">
                                            <textarea type="text" class="form-control" name="client_content" value="{{old('client_content')}}">{{$say->content}}</textarea>
                                            @if($errors->has('client_content'))<span style="color:red;">{{$errors->first('client_content')}}</span>@endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('image') ? ' has-error': ' '}}">
                                        <label class="col-sm-2 control-label {{$errors->has('image')? ' animated shake' : ''}}">Image:</label>
                                        <div class="col-sm-10">
                                            <input type="file" name="image">
                                            @if($errors->has('image'))<span style="color:red;">{{$errors->first('image')}}</span>@endif
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <a href="{{route('clientsSay')}}" class="btn btn-white">Cancel</a>
                                            <button class="btn btn-primary submit" type="submit">Save changes</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection

