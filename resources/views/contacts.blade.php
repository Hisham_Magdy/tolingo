@extends('layouts.master')

@section('title', 'Contact Us')

@section('content')

    <!--============ Start identity ============-->
    <section class="contact-identity">
        <div class="container-fluid"><!--Start Container-->
            <h2>Contact</h2>
        </div><!--End Container-->
    </section>
    <!--============ End identity ============-->

    <!--============ Start get-touch ============-->
    <section class="get-in">
        <div class="overlay"><!--Start overlay-->
            <div class="container"><!--Start Container-->
                <div class="row"><!--Start row-->

                    <div class="col-md-6 col-xs-12">
                        <div class="get-info wow slideInLeft" data-duration="1s">
                            <h3>Get in touch</h3>
                            <p>
                                We are always ready to help to ensure you receive your translations in good time and with the required quality. If you have any questions or problems, we will do our best to resolve your issues as soon as possible. Please, feel free to write to us.
                            </p>
                            <p>
                                We are also always looking for talented translators, so if you would like to share your qualifications with us, please contact us at
                                {{--<b>admin@tolingo.com</b>--}}
                            </p>
                            <ul class="list-unstyled">
                                @php
                                $phones = explode('-', $contacts->phone);
                                $emails = explode('-', $contacts->email);
                                @endphp

                                @foreach($phones as $phone)
                                    <li>
                                        <i class="fa fa-mobile fa-fw"></i>
                                        {{$phone}}
                                    </li>
                                @endforeach

                                @foreach($emails as $email)
                                    <li>
                                        <i class="fa fa-envelope-o fa-fw"></i>
                                        {{$email}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6 col-xs-12">
                        <div class="form-info wow slideInRight" data-duration="1s">
                            <form action="{{route('siteSend')}}" method="post">
                                {{csrf_field()}}
                                <div class="input-group btn-lg btn-block">
                                    <input name="name" class="btn-lg btn-block" type="text" placeholder="Name" required>
                                </div>
                                <div class="input-group btn-lg btn-block">
                                    <input name="email" class="btn-lg btn-block" type="email" placeholder="E-mail" required>
                                </div>
                                <div class="input-group btn-lg btn-block">
                                    <input name="subject" class="btn-lg btn-block" type="text" placeholder="Subject" required>
                                </div>
                                <div class="input-group btn-lg btn-block">
                                    <textarea name="message" class="btn-lg btn-block" placeholder="Message" required></textarea>
                                </div>
                                <input type="submit" value="Send">
                            </form>
                        </div>
                    </div>

                </div><!--End row-->
            </div><!--End Container-->
        </div><!--End overlay-->
    </section>
    <!--============ End get-touch ============-->

    <!--============ Start map ============-->
    <iframe src="{{$contacts->location}}" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
    <!--============ End map ============-->

@endsection

{{--@section('scripts')--}}
    {{--<script>--}}
        {{--$(document).ready(function(){--}}
            {{--$("#contactSubmit").click(function(){--}}
{{--//                alert('sdasdsdasd');--}}
                {{--$.post("{{route('siteSend')}}",--}}
                    {{--{   _token: "{{csrf_field()}}",--}}
                        {{--name: "Donald Duck",--}}
                        {{--city: "Duckburg"--}}
                    {{--},--}}
                    {{--function(data,status){--}}
                        {{--alert("Data: " + data + "\nStatus: " + status);--}}
                    {{--});--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
{{--@endsection--}}