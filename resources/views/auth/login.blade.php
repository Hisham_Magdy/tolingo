<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Tolingo Dashboard | Login</title>
    @include('admin.includes.header')
</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
        <div style="margin-left: -200px">

            <h1 class="logo-name">SINDBAD</h1>

        </div>
        <h3>Welcome to <strong>Tolingo Dashboard</strong></h3>
        {{--<p>Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.--}}
            {{--<!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->--}}
        {{--</p>--}}
        <p>Login in. To see it in action.</p>
        <form class="m-t" role="form" action="{{route('login')}}" method="post">
            {{csrf_field()}}
            <div class="form-group @if($errors->has('email')) has-error @endif">
                <input name="email" type="email" class="form-control" placeholder="Email" required="">
                @if($errors->has('email'))<span style="color:red;">{{$errors->first('email')}}</span>@endif
            </div>
            <div class="form-group @if($errors->has('password')) has-error @endif">
                <input name="password" type="password" class="form-control" placeholder="Password" required="">
                @if($errors->has('password'))<span style="color:red;">{{$errors->first('password')}}</span>@endif
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

            {{--<a href="#"><small>Forgot password?</small></a>--}}
            {{--<p class="text-muted text-center"><small>Do not have an account?</small></p>--}}
            {{--<a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>--}}
        {{--</form>--}}
        {{--<p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>--}}
    </div>
</div>

@include('admin.includes.scripts')

</body>

</html>
